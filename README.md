
## Final Project 

This project is intended as a requirement to get certificate. Developed by Sanber Laravel Advance Batch 18.

Project Includes:
1. UUID as table primary key
2. Separate API and Front-end using Vuejs
3. Broadcast with realtime function
4. Activity Log
5. Authentication & Authorization
6. Mail Notification
7. Payment Gateway (Midtrans)
